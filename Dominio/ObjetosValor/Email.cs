﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ObjetosValor
{
    public class Email
    {
        public Email(string email)
        {
            this.DescricaoEmail = email;
        }

        public string DescricaoEmail { get; private set; }


    }
}
