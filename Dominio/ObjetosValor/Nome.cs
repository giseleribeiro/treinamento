﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.ObjetosValor
{
    public class Nome
    {
        public string primeiroNome { get; set; }
        public string ultimoNome { get; set; }

        public Nome(string primeiroNome, string ultimoNome)
        {
            this.primeiroNome = primeiroNome;
            this.ultimoNome = ultimoNome;
        }
    }
}
