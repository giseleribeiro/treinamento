﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Enum;

namespace Domain.ObjetosValor
{
    public class Documento
    {
        public string Numero { get; private set; }
        public DateTime DataEmissao { get; private set; }
        public ETipoDocumento Tipo { get; private set; }

    }
}
