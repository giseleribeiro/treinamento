﻿using Domain.ObjetosValor;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Validacao
{
    public class NomeValidator : AbstractValidator<Nome>
    {
        public NomeValidator()
        {
            RuleFor(x => x.PrimeiroNome)
                .NotEmpty().WithMessage("Informe o nome do Paciente")
                .Length(3, 100).WithMessage("O primeiro nome deve ter no máximo 20 caracteres");

            RuleFor(x => x.UltimoNome)
                    .NotEmpty().WithMessage("Informe o nome completo do Paciente")
                    .MaximumLength(100).WithMessage("O segundo nome deve ter no máximo 100 caracteres");
        }

    }
}
