﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.ObjetosValor;
using FluentValidation;

namespace Domain.Validacao
{
    public class EnderecoValidator : AbstractValidator<Endereco>
    {
        public EnderecoValidator()
        {
            RuleFor(v => v.Rua)
                .NotEmpty().WithMessage("Informe uma rua para o endereco")
                .MaximumLength(30).WithMessage("O tamanho máximo do nome da rua deve ser 30 caracteres");
                
            RuleFor(v => v.Numero)
                .NotEmpty().WithMessage("Informe um bairro para endereco")
                .Must(NumeroValido).WithMessage("Número inválido");
        }

        private static bool NumeroValido(int numero)
        {
            return numero > 0;
        }


    }
}
