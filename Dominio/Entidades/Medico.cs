﻿using Dominio.ObjetosValor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Entidades
{
    class Medico
    {
        public int Id { get; set; }
        public Nome Nome { get; set; }
        public int CRM { get; set; }
        public string email { get; set; }
        public Telefone telefone { get; set; }

        public Medico(int CRM) { }
    }
}
