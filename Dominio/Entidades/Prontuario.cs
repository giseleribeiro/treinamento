﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Entidades
{
    class Prontuario
    {
        public string tipoSanguineo;
        public string doencasCronicas;
        public string problemasCardiacos;
        public string cirurgiasAnteriores;
        public string diabetes;
        public string doencasDeginerativas;
        public string doencasNeurologicas;
        public string doencasCongenitas;
        public string doencasDeFamilia;

    }
}
