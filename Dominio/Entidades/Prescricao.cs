﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Entidades
{
    class Prescricao
    {
        public Prescricao(long id, DateTime data, IList<MedicamentoPrescrito> medicamentosPrescritos, long medicoId, Medico medico, long pacienteId, Paciente paciente)
        {
            this.id = id;
            this.data = data;
            this.medicamentosPrescritos = medicamentosPrescritos;
            this.medicoId = medicoId;
            this.medico = medico;
            this.pacienteId = pacienteId;
            this.paciente = paciente;
            this.medicamentosPrescritos = new List<MedicamentoPrescrito>();
        }

        public long id { get; set; }
        public DateTime data { get; set; }
        public IList<MedicamentoPrescrito> medicamentosPrescritos { get; set; }


        public long medicoId { get; set; }
        public Medico medico { get; set; }

        public long pacienteId { get; set; }
        public Paciente paciente { get; set; }

    }
}
