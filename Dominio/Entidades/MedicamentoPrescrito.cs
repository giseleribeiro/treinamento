﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Entidades
{
    class MedicamentoPrescrito
    {

        //public virtual

        public string administracao;
        public string dose;
        public string duracao;

        public MedicamentoPrescrito(string administracao, string dose, string duracao)
        {
            this.administracao = administracao;
            this.dose = dose;
            this.duracao = duracao;
        }
    }
}
