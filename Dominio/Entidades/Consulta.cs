﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Entidades
{
    class Consulta
    {

        public long id;
        public DateTime dataAtendimento;
        public DateTime dataRevisao;
        public string diagnostico;
        public string orientacao;

        public Medico medico;

        public RelatorioMedico relatorioMedico;
    
    }
}
