﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Entidades
{
    public class Medicamento
    {
        public int Id { get; set; }
        public string nome { get; set; }
        public string composicao { get; set; }
        public string fabricante { get; set; }
        public bool generico { get; set; }
        public string unidadeMedida { get; set; }


    }
}
